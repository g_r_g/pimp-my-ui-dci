# Pimp my ui
dci version

Le tuning est une pratique consistant à apporter des modifications à un véhicule de série pour le personnaliser (carrosserie, accessoires, moteur…).

"L’observation du tuning comme pratique fait également apparaître l’idée de « pimper », c’est-à-dire d’apporter des modifications personnelles, d’extraire l’objet d’une série commerciale pour le rendre unique, de le faire entrer en conformité, non plus avec un standard fixé par l’industrie et le marketing, mais avec un idéal individuel esthétique et technique."
